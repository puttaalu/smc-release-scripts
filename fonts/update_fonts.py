#!/usr/bin/env python3

# Assumption : Works with gitlab api v4.
# python3 requests and configparser modules.

# CONFIG file.

# ini format.
# - contains font names and corresponding base urls.
# - contains location of base font directory.

# 1. Get the list of fonts.
#    - ensure each font's directory exists in the root of releases.smc.org.in/fonts/
# 2. For each font
#    - fetch the release list
#    - list release tags.
#    - if current font's directory does not contain tag
#      - if links is empty, return.
#      - create directory with the tag name.
#      - fetch the target of each object in links array.
#      - re point the "LATEST" link to the newly created tag.

import configparser
import os
import requests
import urllib.parse
import json
import sys
from datetime import datetime


def print_config(config):
    print("Base font release directory :" + base_directory)
    print("Base URL : " + base_url)
    print("Configured fonts")
    for key in config['fonts']:
        print(key + " : " + config['fonts'][key])

def ensure_font_directory_exists(base_dir,font_name):
    path = os.path.join(base_dir,font_name)
    if not os.path.exists(path):        
        os.makedirs(path)

def fetch_releases(font, base_url):
    font_path = urllib.parse.quote(font,safe='')
    url = base_url + font_path + "/releases"
    response = requests.get(url)
    if response.status_code == 200 :
        return response.text
    return ""

def download_asset(link, release_path):
    url = link["direct_asset_url"]
    path = os.path.join( release_path, link["name"])
    
    r = requests.get(url, allow_redirects=True)
    open(path,'wb').write(r.content)

def make_release(base_dir,font_name,release):
    tag_name = release["tag_name"]
    release_path = os.path.join(base_dir, font_name, tag_name)
    # check if this release exists
    # Assume that we already did everything needed.
    if os.path.exists(release_path):        
        return 0
    os.makedirs(release_path)
    for link in release["assets"]["links"]:
        download_asset(link,release_path)
    # create a VERSION file.
    f = open( os.path.join(release_path, "VERSION"), "w")
    f.write(tag_name)
    f.close()

    return 0

def create_latest_symlink(base_dir,font_name,tag_name):
    if tag_name == "" :
        return 0

    symlink_path = os.path.join(base_dir , font_name , "LATEST")
    release_path = os.path.join(base_dir , font_name , tag_name)

    # if the LATEST symlink exists, remove it.
    if os.path.exists(symlink_path) and os.path.islink(symlink_path):
        os.unlink(symlink_path)
    # create the symlink
    os.symlink(release_path, symlink_path, True)
    return 0

# Accept a config file as an argument

config_file_path = "font_update_config.ini"

if len(sys.argv) > 1 :
    config_file_path = sys.argv[1]

print("Config file : " + config_file_path)

config = configparser.ConfigParser()
config.read(config_file_path)

base_directory = config['main']['base_directory']
base_url = config['main']['base_url']

# debug
# print_config(config)

for font in config['fonts']:
    font_path = config['fonts'][font]
    ensure_font_directory_exists(base_directory,font)
    release_json = fetch_releases(font_path,base_url)
    releases = json.loads(release_json)
    latest_release = ""
    latest_release_date = datetime(2004, 1, 1)

    for release in releases:
        tag_name = release["tag_name"]
        asset_count = len(release["assets"]["links"])
        released_at = datetime.strptime(release["released_at"],'%Y-%m-%dT%H:%M:%S.%fZ')
        if asset_count > 0:
            make_release(base_directory, font, release)
            # check if this release later than the last one.
            if released_at > latest_release_date:
                latest_release_date = released_at
                latest_release = tag_name                

    create_latest_symlink(base_directory, font, latest_release)
