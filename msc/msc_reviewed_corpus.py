#!/usr/bin/env python3

# Assumption : Works with gitlab api v4.
# python3 requests and configparser modules.

# CONFIG file.

# ini format.
# - contains msc speech corpus path
# - contains location of base font directory.

#    - fetch the release list
#    - list release tags.
#      - if links is empty, return.
#      - create directory with the tag name.
#      - fetch the target of each object in links array.
#      - extract the 
#      - re point the "LATEST" link to the newly created tag.

import configparser
import os
import requests
import urllib.parse
import json
import sys
from datetime import datetime


def print_config(config):
    print("Base font release directory :" + base_directory)
    print("Base URL : " + base_url)
    print("Configured fonts")
    for key in config['fonts']:
        print(key + " : " + config['fonts'][key])

def ensure_release_directory_exists(base_dir,font_name):
    path = os.path.join(base_dir,font_name)
    if not os.path.exists(path):        
        os.makedirs(path)

def fetch_releases(font, base_url):
    font_path = urllib.parse.quote(font,safe='')
    url = base_url + font_path + "/releases"
    print (url)
    response = requests.get(url)
    if response.status_code == 200 :
        return response.text
    print("got nothing")
    return ""

def download_asset(link, release_path, tag_name):
    url = link["url"]
    path = os.path.join( release_path, tag_name + ".zip")
    print("DOWNLOADING ASSET to PATH " + str(path))
    r = requests.get(url, allow_redirects=True)
    open(path,'wb').write(r.content)
    
def cleanup_asset(release_path,tag_name):
    path = os.path.join( release_path, tag_name + ".zip")
    archive_directory = release_directory +"-"+tag_name
    cleaned_up_asset_path = os.path.join( release_path, archive_directory + ".zip")
    command = "zip -q --copy " + path + " " + archive_directory + "/audio/* " + archive_directory + "/metadata.tsv " + archive_directory + "/README.md" + " --out " +cleaned_up_asset_path
    print("CLEANING UP ARCHIVE")
    os.system(command)
    # Delete the original
    print("REMOVING THE ORIGINAL")
    os.system("rm " + path)
    
def make_release(base_dir,release_directory,release):
    tag_name = release["tag_name"]
    release_path = os.path.join(base_dir, release_directory, tag_name)
    # check if this release exists
    # Assume that we already did everything needed.
    print(release_path)
    if os.path.exists(release_path):        
        return 0
    os.makedirs(release_path)
    for link in release["assets"]["sources"]:
        # Check if it is a zip file. if yes download.
        if link["format"] == "zip":
            download_asset(link,release_path,tag_name)
            cleanup_asset(release_path,tag_name)
    # create a VERSION file.
    f = open( os.path.join(release_path, "VERSION"), "w")
    f.write(tag_name)
    f.close()

    return 0

def create_latest_symlink(base_dir,font_name,tag_name):
    if tag_name == "" :
        return 0

    symlink_path = os.path.join(base_dir , font_name , "LATEST")
    release_path = os.path.join(base_dir , font_name , tag_name)

    # if the LATEST symlink exists, remove it.
    if os.path.exists(symlink_path) and os.path.islink(symlink_path):
        os.unlink(symlink_path)
    # create the symlink
    os.symlink(release_path, symlink_path, True)
    return 0

# Accept a config file as an argument

config_file_path = "msc_reviewed_corpus.ini"

if len(sys.argv) > 1 :
    config_file_path = sys.argv[1]

print("Config file : " + config_file_path)

config = configparser.ConfigParser()
config.read(config_file_path)

base_directory = config['main']['base_directory']
base_url = config['main']['base_url']

# debug
# print_config(config)
reviewed_speech_repo = config['msc']['reviewed_speech_repo']
release_directory = config['msc']['release_directory']
ensure_release_directory_exists(base_directory,release_directory)
release_json = fetch_releases(reviewed_speech_repo,base_url)
releases = json.loads(release_json)

latest_release = ""
latest_release_date = datetime(2004, 1, 1)

for release in releases:
    tag_name = release["tag_name"]
    asset_count = len(release["assets"]["links"])
    released_at = datetime.strptime(release["released_at"],'%Y-%m-%dT%H:%M:%S.%fZ')
#    if asset_count > 0:
    make_release(base_directory, release_directory, release)
    # check if this release later than the last one.
    if released_at > latest_release_date:
        latest_release_date = released_at
        latest_release = tag_name                
            
create_latest_symlink(base_directory, release_directory, latest_release)
